﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Super_Salki
{
    [TestFixture]
    internal class FizzBuzzOtherGroup
    {
        public List<string> ZwrocListe()
        {
            var lista = new List<string>();
            for (var i = 0; i < 100; i++)
                switch (i % 3)
                {
                    case 0 when i % 5 == 0:
                        lista.Add("FizzBuzz");
                        break;
                    case 0:
                        lista.Add("Fizz");
                        break;
                    default:
                        if (i % 5 == 0)
                            lista.Add("Buzz");
                        else
                            lista.Add(i.ToString());
                        break;
                }
            return lista;
        }

        [Test]
        public void CzyListaZawieraStoElementow()
        {
            var sto = 100;
            var result = ZwrocListe();

            Assert.AreEqual(result.Count(), sto);
        }

        [Test]
        public void PowinnaZwrocicBuzzJesliPodzielnaPrzezPiec()
        {
            var wynik = "Buzz";

            var lista = ZwrocListe();

            Assert.AreEqual(lista[5], wynik);
        }

        [Test]
        public void PowinnaZwrocicFizzBuzzJesliPodzielnaPrzezTrzyiPiec()
        {
            var wynik = "FizzBuzz";

            var lista = ZwrocListe();

            for (var i = 0; i < lista.Count; i++)
                if (i % 3 == 0 && i % 5 == 0)
                    Assert.AreEqual(wynik, lista[i]);
        }

        [Test]
        public void PowinnaZwrocicFizzJesliJestPodzielnaPrzezTrzy()
        {
            var wynik = "Fizz";

            var lista = ZwrocListe();

            Assert.AreEqual(lista[3], wynik);
        }
    }
}