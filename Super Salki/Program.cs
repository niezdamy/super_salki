using System;
using System.Collections.Generic;
using System.Linq;

namespace Super_Salki
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var strefyRelaksu = GenerujStrefyRelaksu();
            var salki = GenerujSalki();

            //wyświetl salki na 6 piętrze

            var salkiNaSzostym = salki.Where(s => s.Piętro == 6);

            foreach (var salka in salkiNaSzostym)
                Console.WriteLine(salka.PrzedstawSie());

            //posortowane, otwarte strefy relaksu, wypisane ForEachem na kolekcji

            var listaOtwartychStref = new List<StrefaRelaksu>();

            listaOtwartychStref.AddRange(strefyRelaksu
                .Where(s => s.CzyDziala)
                .OrderBy(s => s.Piętro));

            strefyRelaksu.ForEach(x => x.SprawdzCzyOtwarta());

            //lista Otwartych Pomieszczeń po interfejsie IOtwarty, rzutowanie na typ IOtwarty w linii 37 - pomocne, ale w tym wypadku niekonieczne :) 

            var listaOtwartychPomieszczen = new List<IOtwarty>();

            listaOtwartychPomieszczen.AddRange(salki
                .Where(x => x.CzyDziala)
                .Select(c => (IOtwarty) c));

            listaOtwartychPomieszczen.AddRange(strefyRelaksu
                .Where(x => x.CzyDziala)
                .Select(r => r));

            listaOtwartychPomieszczen.ForEach(c => c.SprawdzCzyOtwarta());

            Console.ReadLine();
        }

        private static List<StrefaRelaksu> GenerujStrefyRelaksu()
        {
            var strefa1 = new StrefaRelaksu
            {
                Nazwa = "Piłkarzyki na 4",
                CzyDziala = true,
                Budynek = "Sern",
                Piętro = 4
            };

            var strefa2 = new StrefaRelaksu
            {
                Nazwa = "MORTAL WOMBATTTT",
                CzyDziala = true,
                Budynek = "Alder",
                Piętro = 3
            };

            var ListaStrefRelaksu = new List<StrefaRelaksu>();

            ListaStrefRelaksu.Add(strefa1);
            ListaStrefRelaksu.Add(strefa2);

            return ListaStrefRelaksu;
        }

        private static List<Salka> GenerujSalki()
        {
            var salka1 = new Salka
            {
                Nazwa = "Death Star",
                CzyDziala = true,
                Budynek = "Stern",
                IloscOsob = 12,
                Piętro = 6,
                Telewizor = true
            };

            var salka2 = new Salka
            {
                Nazwa = "Dune",
                CzyDziala = true,
                Budynek = "Stern",
                IloscOsob = 8,
                Piętro = 5,
                Telewizor = true
            };

            var ListaSalek = new List<Salka>();

            ListaSalek.Add(salka1);
            ListaSalek.Add(salka2);

            return ListaSalek;
        }
    }
}