﻿using System;

namespace Super_Salki
{
    class Salka : Pomieszczenie, IPrzedstawienie, IOtwarty
    {
        public string Nazwa;
        public int IloscOsob;
        public bool Telewizor;
        public bool CzyDziala;

        public string PrzedstawSie()
        {
            return "Jestem salką, nazywam się: " + Nazwa;
        }

        public void SprawdzCzyOtwarta()
        {
            if (CzyDziala )
            {
                Console.WriteLine("Jestem otwartą salką o nazwie: " + Nazwa);
            }
            else
            {
                Console.WriteLine("Jestem zamkniętą salką o nazwie: " + Nazwa);
            }
        }

    }
}
