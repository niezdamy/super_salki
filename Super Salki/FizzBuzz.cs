﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Super_Salki
{
    [TestFixture]
    internal class FizzBuzz
    {
        private IEnumerable<int> GenerateList()
        {
            var result = Enumerable.Range(1, 100);
            return result;
        }

        private string CheckFizz(int n)
        {
            if (n % 3 == 0)
                return "Fizz";
            return "";
        }

        private string CheckBuzz(int n)
        {
            if (n % 5 == 0)
                return "Buzz";
            return "";
        }

        [Test]
        public void DzielonePrzez3()
        {
            var result = CheckFizz(3);
            Assert.AreEqual(result, "Fizz");
        }

        [Test]
        public void DzelonePrzez5()
        {
            var result = CheckBuzz(5);
            Assert.AreEqual(result, "Buzz");
        }

        [Test]
        public void DzielonePrzez3i5()
        {
            var expected = "FizzBuzz";
            var actual = CheckFizz(15) + CheckBuzz(15);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ListaMa100Elementow()
        {
            var result = GenerateList();
            Assert.AreEqual(result.Count(), 100);
        }
    }
}