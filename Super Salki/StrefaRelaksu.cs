﻿using System;

namespace Super_Salki
{
    class StrefaRelaksu : Pomieszczenie, IPrzedstawienie, IOtwarty
    {
        public string Nazwa;
        public bool CzyDziala;

        public string PrzedstawSie()
        {
            return "Jestem strefą relaksu, nazywam się: " + Nazwa;
        }

        public void SprawdzCzyOtwarta()
        {
            if (CzyDziala)
            {
                Console.WriteLine("Jestem otwartą salką o nazwie: " + Nazwa);
            }
            else
            {
                Console.WriteLine("Jestem zamkniętą salką o nazwie: " + Nazwa);
            }
        }
    }
}
